-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 03, 2018 lúc 02:09 AM
-- Phiên bản máy phục vụ: 10.1.30-MariaDB
-- Phiên bản PHP: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `manh`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `introductions`
--

CREATE TABLE `introductions` (
  `id` int(10) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `introductions`
--

INSERT INTO `introductions` (`id`, `content`) VALUES
(1, '<p>123</p>\r\n');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `peoples`
--

CREATE TABLE `peoples` (
  `id` int(10) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` int(10) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `image`, `created`, `modified`, `user_id`) VALUES
(1, 'Thư ngỏ: Kêu gọi và chào đón các bạn tân sinh viên khóa 10', 'sadsa', 'sssss.jpg', '2018-10-02 23:24:21', '2018-09-25 08:43:18', 1),
(2, 'Thư ngỏ: Kêu gọi và chào đón các bạn tân sinh viên khóa 10', '<p>Kh&aacute;nh Đạt</p>\r\n', 'trx_20189279193424292.jpg', '2018-09-27 09:01:34', '2018-09-27 09:01:34', 1),
(3, 'Hà Nội ơi, ta yêu mi được không ?', '<p>H&agrave; Nội ơi, ta y&ecirc;u mi được kh&ocirc;ng ?</p>\r\n', '42819709_1523230821109791_3317080633716506624_o_2018102145914034766.jpg', '2018-10-02 14:59:00', '2018-10-02 14:59:00', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `professors`
--

CREATE TABLE `professors` (
  `id` int(10) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `avatar`, `created`, `modified`) VALUES
(1, 'tien dat', 'admin@yahoo.com', '$2y$10$PZ9A1r1tyhrAn8ZvDnXUvuRiM8rW6kgV7689wflLkdaqByEpdsxgq', '1_2_20189272923191623.jpg', '2018-09-27 02:09:31', '2018-09-27 02:09:31'),
(2, 'khánh', 'khanh@gmail.com', '$2y$10$saINoOTuJVIMQoaFS0fSkerwjNe.6ASB4/2uO/LfGa5Hz113DTrBi', '1', '2018-09-25 09:45:02', '2018-09-25 09:45:02'),
(3, 'cường', 'cuong@mail.com', '$2y$10$mJ2yTSaW4AhbdQvFsm23CewjGUZQGk6VTyq1ovs2zOu/K3fBaYU0W', '1.jpg', '2018-09-25 15:31:31', '2018-09-25 15:31:31'),
(4, 'dương456', 'duong@mail.com', '$2y$10$8cco1Yd1hndac1ZmgrNzBeXhV.jyA/5HtCvy32/b1/KPDqlQTUMhu', '11_2018926737665750.png', '2018-09-26 11:13:38', '2018-09-26 11:13:38'),
(5, 'Tien Dat Bui 123', 'buitiendat2009@yahoo.com', '$2y$10$Hh9TuM6TMqv8uY8E0qShgOdgi.w3ivXgVDmSxUlx9/puTG5mJ1Tl6', '2_20189272723269772.jpg', '2018-09-27 02:07:32', '2018-09-27 02:07:32');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `introductions`
--
ALTER TABLE `introductions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `peoples`
--
ALTER TABLE `peoples`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `professors`
--
ALTER TABLE `professors`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `introductions`
--
ALTER TABLE `introductions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `peoples`
--
ALTER TABLE `peoples`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `professors`
--
ALTER TABLE `professors`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
