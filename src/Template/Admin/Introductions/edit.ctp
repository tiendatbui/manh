<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Introduction $introduction
 */
?>
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $introduction->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $introduction->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Introductions'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="introductions form large-10 medium-8 columns content">
    <?= $this->Form->create($introduction) ?>
    <fieldset>
        <legend><?= __('Edit Introduction') ?></legend>
        <?php
            echo $this->Form->control('content', ['class'=>'ckeditor']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
