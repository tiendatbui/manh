<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IntroductionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IntroductionsTable Test Case
 */
class IntroductionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IntroductionsTable
     */
    public $Introductions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.introductions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Introductions') ? [] : ['className' => IntroductionsTable::class];
        $this->Introductions = TableRegistry::getTableLocator()->get('Introductions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Introductions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
